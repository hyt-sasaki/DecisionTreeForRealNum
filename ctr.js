$(function () {
    /* 訓練データの設定 */
    var RANGE = [20, 20];   //座標系の定義域幅
    var COLOR_T = 'rgb(255, 0, 0)';
    var COLOR_F = 'rgb(0, 0, 255)';
    var COLORS = [COLOR_F, COLOR_T];
    var CLASSNAMES = ['負例', '正例'];
    (function () {
        var axesCanvas = $('#axesCanvas')[0];
        drawAxes(axesCanvas, RANGE);
    })();

    var dataRef = {Data:null};
    var fileRef = {file:null};
    var dataCanvas = $('#dataCanvas')[0];
    var modelCanvas = $('#modelCanvas')[0];
    var treeCanvas = $('#treeCanvas')[0];

    /* イベントハンドラの設定 */
    $('#dataCanvas').on('click', {dataRef:dataRef, canvas:dataCanvas, range:RANGE, colors:COLORS}, onCanvasClicked);
    $('#learn').on('click', {dataRef:dataRef, canvas:modelCanvas, treeCanvas:treeCanvas, range:RANGE, colors:COLORS, classNames:CLASSNAMES}, onTrain);
    $('#resetButton').on('click', {dataRef:dataRef, dataCanvas:dataCanvas, modelCanvas:modelCanvas}, onReset);
    $('#genFromFile').on('click', {dataRef:dataRef, fileRef:fileRef, canvas:dataCanvas, range:RANGE, colors:COLORS}, onGenerateFromFile);
    $('#download').on('click', {dataRef:dataRef}, onDownload);
    $('#openFile').on('change', {fileRef:fileRef}, onFileOpen);
    $(window).on('keydown', onShiftkeyDown);
    $(window).on('keydown', onEnterkeyDown);
});

function onShiftkeyDown(event) {
    if (event.shiftKey) {
        if($('input[name="clickLabel"]:eq(0)').prop('checked')) {
            $('input[name="clickLabel"]:eq(1)').prop('checked', true);
        } else {
            $('input[name="clickLabel"]:eq(0)').prop('checked', true);
        }
    }
}

function onEnterkeyDown(event) {
    if (event.which === 13) {
        console.log('Enter');
        $('#learn').trigger('click');
    }
}

function onCanvasClicked(event) {
    var canvas = event.data.canvas;
    var range = event.data.range;
    var canvasRect = canvas.getBoundingClientRect();
    var colors = event.data.colors;
    var cx = event.clientX - canvasRect.left;
    var cy = event.clientY - canvasRect.top;
    var c = [cx, cy];
    var x = canvasToData(c, canvas, range);
    var y = ($('input[name="clickLabel"]:checked').val() === "positive") ? 1: 0;
    var d = {x:x, y:y};

    if (event.data.dataRef.Data === null) {
        event.data.dataRef.Data = [];
    }
    event.data.dataRef.Data.push(d);
    drawPoint(d, canvas, range, colors);
}

function onTrain(event) {
    alert('学習開始');
    var colors = event.data.colors;
    var classNum = colors.length;
    var classNames = event.data.classNames;
    var range = event.data.range;
    var canvas = event.data.canvas;
    var treeCanvas = event.data.treeCanvas;
    var Data = event.data.dataRef.Data;
    var W = [];
    for (var i=0; i < Data.length; ++i) {
        W.push(1 / Data.length);
    }

    var maxDepth = Number($('#depth').val());
    var decisionTree = new DecisionTree(classNum, average, W, maxDepth);
    decisionTree.train(Data);

    out = $('#output span');
    out.children().remove();
    out.append($('<sapn>').html('<br />$N_{error} = ' + (decisionTree.check(event.data.dataRef.Data) + '$')));

    MathJax.Hub.Queue(["Typeset", MathJax.Hub, out.id]);

    canvas.getContext('2d').clearRect(0, 0, canvas.width, modelCanvas.height);
    drawDiscriminant(event.data.dataRef.Data, canvas, range, decisionTree, colors);

    var nd = new NodeDrawable(decisionTree.root, 0);
    nd.calcLayout();
    var rectWidth = calcNodeWidth(classNames.concat(['不純度:' + String(9.999)]), treeCanvas);
    var rectHeight = 40;
    calcCanvasSize(nd, rectWidth, rectHeight, treeCanvas);
    clearCanvas(treeCanvas);
    drawTree(nd, classNames, treeCanvas, rectWidth, rectHeight);
}

function onReset(event) {
    event.data.dataRef.Data = [];
    var dataCanvas = event.data.dataCanvas;
    var modelCanvas = event.data.modelCanvas;
    dataCanvas.getContext('2d').clearRect(0, 0, dataCanvas.width, dataCanvas.height);
    modelCanvas.getContext('2d').clearRect(0, 0, modelCanvas.width, modelCanvas.height);
    out = $('#output span');
    out.children().remove();
}

function generateColors(n) {

}

function calcNodeWidth(labels, canvas) {
    var ctx = canvas.getContext('2d');
    ctx.font = '18px Century Gothic';
    var maxWidth = 0;
    var metrics;
    for (var i=0; i < labels.length; ++i) {
        metrics = ctx.measureText(labels[i]);
        if (metrics.width > maxWidth) {
            maxWidth = metrics.width;
        }
    }
    return maxWidth * 1.2;
}

function calcMaxXYEnclosure(nd) {
    var maxX = 0;
    var maxY = 0;
    function calcMaxXY(nd) {
        if (!nd.nodeType) {
            for (var i=0; i < nd.children.length; ++i) {
                if (nd.children[i].x > maxX) {
                    maxX = nd.children[i].x;
                }
                if (nd.children[i].y > maxY) {
                    maxY = nd.children[i].y;
                }
                calcMaxXY(nd.children[i]);
            }
        }
    }
    calcMaxXY(nd);
    return [maxX, maxY];
}

function calcCanvasSize(nd, rectWidth, rectHeight, canvas) {
    var maxXY = calcMaxXYEnclosure(nd);
    canvas.width = rectWidth * (maxXY[0] + 2)
    canvas.height = 4 * rectHeight * (maxXY[1] + 1)
}
