/* 点の描画
* d: データ
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawPoint(d, canvas, r, colors) {
    var ctx = canvas.getContext('2d');
    var c = dataToCanvas(d.x, canvas, r);

    ctx.beginPath();
    ctx.strokeStyle = colors[Number(d.y)];
    ctx.arc(c[0], c[1], canvas.width / 125, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
}

/* 点群の描画
* Data: データ集合
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawPoints(Data, canvas, r, colors) {
    for (var i=0; i < Data.length; ++i) {
        var d = Data[i];
        drawPoint(d, canvas, r, colors);
    }
}

/* 識別領域の描画
*
*/
function drawDiscriminant(Data, canvas, r, dt, colors) {
    var xNum = 100;
    var yNum = 100;
    var rectWidth = canvas.width / xNum;
    var rectHeight = canvas.height / yNum;
    for (var i=0; i < xNum; ++i) {
        for (var j=0; j < yNum; ++j) {
            var rectLeft = rectWidth * i;
            var rectTop = rectHeight * j;
            var c = getRectCenter(rectWidth, rectHeight, rectLeft, rectTop);
            var x = canvasToData(c, canvas, r);
            var cls = dt.root.predict(x);

            var color = colors[dt.predict(x)];
            drawCross(rectWidth, rectHeight, rectLeft, rectTop, canvas, color);
        }
    }
}

function drawCross(w, h, l, t, canvas, color) {
    /* バツ印の左上座標 */
    var x1 = l;
    var y1 = t;
    /* バツ印の右下座標 */
    var x2 = l + w;
    var y2 = t + h;
    /* バツ印の右上座標 */
    var x3 = l + w;
    var y3 = t;
    /* バツ印の左下座標 */
    var x4 = l;
    var y4 = t + h;

    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    ctx.moveTo(x1, y1);
    ctx.lineTo(x2, y2);
    ctx.moveTo(x3, y3);
    ctx.lineTo(x4, y4);
    ctx.strokeStyle = color;
    ctx.stroke();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
}

function getRectCenter(w, h, l, t) {
    return [l + w / 2, t + h / 2];
}

/* 座標軸の描画
* canvas: canvasオブジェクト
* r: 座標系の定義幅
*/
function drawAxes(canvas, r) {
    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    /* x軸の描画 */
    ctx.moveTo(0, canvas.height / 2);
    ctx.lineTo(canvas.width, canvas.height / 2);
    ctx.moveTo(canvas.width, canvas.height / 2);
    ctx.lineTo(canvas.width * 108 / 110, canvas.height * 53 / 110);
    ctx.lineTo(canvas.width * 108 / 110, canvas.height * 57 / 110);
    ctx.closePath();
    ctx.moveTo(canvas.width * 105 / 110, canvas.height * 54 / 110);
    ctx.lineTo(canvas.width * 105 / 110, canvas.height * 56 / 110);
    ctx.moveTo(canvas.width * 5 / 110, canvas.height * 54 / 110);
    ctx.lineTo(canvas.width * 5 / 110, canvas.height * 56 / 110);

    /* y軸の描画 */
    ctx.moveTo(canvas.width / 2, canvas.height);
    ctx.lineTo(canvas.width / 2, 0);
    ctx.moveTo(canvas.width / 2, 0);
    ctx.lineTo(canvas.width * 53 / 110, canvas.height * 2 / 110);
    ctx.lineTo(canvas.width * 57 / 110, canvas.height * 2 / 110);
    ctx.closePath();
    ctx.moveTo(canvas.width * 54 / 110, canvas.height * 5 / 110);
    ctx.lineTo(canvas.width * 56 / 110, canvas.height * 5 / 110);
    ctx.moveTo(canvas.width * 54 / 110, canvas.height * 105 / 110);
    ctx.lineTo(canvas.width * 56 / 110, canvas.height * 105 / 110);

    ctx.stroke();
    ctx.fill();

    /* 文字の描画 */
    var normalSize = Math.min(canvas.width, canvas.height) / 20;
    var subscriptSize = Math.floor(normalSize * 0.5);
    var fontname = String(normalSize) + "px 'Times New Roman'";
    var _fontname = String(subscriptSize) + "px 'Times New Roman'";
    var fontnameItalic = "Italic " + fontname
    var _fontnameItalic = "Italic " + _fontname

    ctx.font = fontnameItalic;
    ctx.textAlign = 'left';
    ctx.textBaseline = 'middle';
    ctx.fillText('x', canvas.width * 58 / 110, canvas.height * 2 / 110);
    ctx.font = _fontname;
    ctx.textBaseline = 'bottom';
    var metrics = ctx.measureText('x');
    ctx.fillText('2', canvas.width * 58 / 110 + metrics.width * 1.5, (canvas.height * 2 / 110) + normalSize / 2);
    ctx.textAlign = 'right';
    ctx.textBaseline = 'middle';
    ctx.font = fontname;
    ctx.fillText(String(r[0] / 2), canvas.width * 52 / 110, canvas.height * 5 / 110);
    ctx.fillText('-' + String(r[0] / 2), canvas.width * 52 / 110, canvas.height * 105 / 110);

    ctx.font = fontnameItalic;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'bottom';
    ctx.fillText('x', canvas.width * 108 / 110, canvas.height * 52 / 110);
    ctx.font = _fontname;
    ctx.fillText('1', canvas.width * 108 / 110 + metrics.width * 1.1, (canvas.height * 52 / 110));

    ctx.font = fontname;
    ctx.textAlign = 'center';
    ctx.textBaseline = 'top';
    ctx.fillText(String(r[1] / 2), canvas.width * 105 / 110, canvas.height * 58 / 110);
    ctx.fillText('-' + String(r[1] / 2), canvas.width * 5 / 110, canvas.height * 58 / 110);
    ctx.textAlign = 'right';
    ctx.textBaseline = 'top';
    ctx.fillText('0', canvas.width * 54 / 110, canvas.height * 56 / 110);
}

/* 座標系の変換 */
function dataToCanvas(x, canvas, r) {
    var cx = canvas.width * (1 / 2 + x[0] / r[0]);
    var cy = canvas.height * (1 / 2 - x[1] / r[1]);
    return [cx, cy];
}

function canvasToData(c, canvas, r) {
    x = r[0] * (c[0] / canvas.width - 1 / 2);
    y = r[1] * (-c[1] / canvas.height + 1 / 2);
    return [x, y];
}

function drawTree(dn, classNames, canvas, rectWidth, rectHeight) {
    var line_dx = rectWidth;
    var line_dy = rectHeight * 2;
    var text;

    if (dn.nodeType) {
        text = classNames[dn.node.getMaxProportionClass()];
    } else {
        text = 'x' + String(dn.node.theta + 1);
    }

    if (!(dn.nodeType)) {
        var base_c = calcNodeBasePosition(dn.x, dn.y, line_dx, line_dy, rectWidth, rectHeight);

        var ln = dn.children[0];
        var left_c = calcNodePosition(ln.x, ln.y, line_dx, line_dy, rectWidth, rectHeight);
        var rn = dn.children[1];
        var right_c = calcNodePosition(rn.x, rn.y, line_dx, line_dy, rectWidth, rectHeight);

        //connectionTextLeft = '< ' + String(dn.node.threshold.toFixed(3));
        //connectionTextRight = '>= ' + String(dn.node.threshold.toFixed(3));
        connectionTextLeft = '< ' + numToString(dn.node.threshold);
        connectionTextRight = '>= ' + numToString(dn.node.threshold);
        drawConnection(base_c, left_c, connectionTextLeft, canvas);
        drawConnection(base_c, right_c,connectionTextRight, canvas);

        drawTree(ln, classNames, canvas, rectWidth, rectHeight);
        drawTree(rn, classNames, canvas, rectWidth, rectHeight);
    }

    var node_c = calcNodePosition(dn.x, dn.y, line_dx, line_dy, rectWidth, rectHeight);
    drawNode(node_c, dn, text, canvas, rectWidth, rectHeight);
}

function calcNodePosition(x, y, line_dx, line_dy, nodeWidth, nodeHeight) {
    return [line_dx * x + nodeWidth, (line_dy + nodeHeight) * y + nodeHeight / 2];
}

function calcNodeBasePosition(x, y, line_dx, line_dy, nodeWidth, nodeHeight) {
    return [line_dx * x + nodeWidth, (line_dy + nodeHeight) * y + nodeHeight * 3 / 2];
}

function drawNode(c, node, label, canvas, rectWidth, rectHeight) {
    var ctx = canvas.getContext('2d');
    ctx.strokeStyle = 'rgb(0, 0, 0)';
    if (node.nodeType) {
        ctx.strokeRect(c[0] - rectWidth / 2, c[1], rectWidth, rectHeight);
    }

    var normalSize = Math.min(rectWidth, rectHeight) / 2.3;
    var subscriptSize = Math.floor(normalSize * 0.5);
    var fontname = String(normalSize) + "px 'Times New Roman'";
    var fontnameMS = String(normalSize) + "px 'MS Pゴシック'";
    var _fontname = String(subscriptSize) + "px 'Times New Roman'";
    var fontnameItalic = "Italic " + fontname
    var _fontnameItalic = "Italic " + _fontname
    if (node.nodeType) {
        ctx.font = fontnameMS;
    } else {
        ctx.font = fontnameItalic;
    }
    ctx.textAlign = 'center';
    ctx.textBaseline = 'hanging';
    ctx.fillStyle = 'rgb(0, 0, 0)';
    ctx.fillText(label, c[0], c[1], rectWidth);
    ctx.font = fontnameMS;
    //ctx.fillText('不純度: ' + (node.node.impureness).toFixed(3), c[0], c[1] + rectHeight / 2, rectWidth);
    ctx.fillText('不純度: ' + numToString(node.node.impureness), c[0], c[1] + rectHeight / 2, rectWidth);
}

function drawConnection(c1, c2, text, canvas) {
    var metrics = canvas.getContext('2d').measureText(text);
    var rectWidth = metrics.width * 1.2;
    var rectHeight = 20;
    var ctx = canvas.getContext('2d');

    ctx.beginPath();
    ctx.strokeStyle = 'rgb(0, 0, 0)';
    ctx.moveTo(c1[0], c1[1]);
    ctx.lineTo(c2[0], c2[1]);
    ctx.stroke();

    var cc = [(c1[0] + c2[0]) / 2, (c1[1] + c2[1]) / 2];
    ctx.fillStyle = 'rgb(255, 255, 255)';
    ctx.fillRect(cc[0] - rectWidth / 2, cc[1] - rectHeight / 2, rectWidth, rectHeight);

    ctx.font = '18px Century Gothic';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    ctx.fillStyle = 'rgb(0, 0, 0)';
    ctx.fillText(text, cc[0], cc[1]);
}

var NodeDrawable = (function () {
    var NodeDrawable = function (node, depth) {
        this.x = -1;
        this.y = depth;
        this.node = node;
        this.mod = 0;
        this.initialize();
    }

    var pt = NodeDrawable.prototype;

    pt.initialize = function () {
        this.children = [];
        if (this.node.children.length !== 0) {
            this.nodeType = false;  //ノードタイプ(true: 葉ノード, false: 枝ノード)
            for (var i=0; i < this.node.children.length; ++i) {
                var child = new NodeDrawable(this.node.children[i], this.y + 1);
                this.children.push(child);
            }
        } else {
            this.nodeType = true;   //ノードタイプ(true: 葉ノード, false: 枝ノード)
        }
    }

    pt.calcLayout = function () {
        this.setup(0, [], []);
        this.addmods(0);
    }

    pt.setup = function (depth, nexts, offset) {
        if (nexts[depth] === undefined) {
            nexts[depth] = 0;
        }
        if (offset[depth] === undefined) {
            offset[depth] = 0;
        }
        for (var i=0; i < this.children.length; ++i) {
            this.children[i].setup(depth + 1, nexts, offset);
        }

        //this.node.y = depth;

        var place;

        if (this.nodeType) {
            place = nexts[depth];
            this.x = place;
            nexts[depth] += 2;
        } else {
            place = 0;
            for (var i=0; i < this.children.length; ++i) {
                place += this.children[i].x;
            }
            place /= this.children.length;
            offset[depth] = Math.max(offset[depth], nexts[depth] - place);
            place += offset[depth];
            nexts[depth] = place + 2;
        }

        this.x = place;

        this.mod = offset[depth];
    }

    pt.addmods = function (mod) {
        this.x += mod;

        for (var i=0; i < this.children.length; ++i) {
            this.children[i].addmods(mod + this.mod);
        }
    }

    return NodeDrawable;
})();

function clearCanvas(canvas) {
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function numToString(n) {
    var n_str;
    if (Math.abs(n) >= 1 / 100) {
        n_str = n.toFixed(3);
    } else if (n === 0) {
        n_str = '0';
    } else {
        n_str = n.toExponential(3);
    }

    return n_str;
}
